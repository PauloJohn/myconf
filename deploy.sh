#!/bin/bash

# vim
ln -s ~/myconf/vimrc.conf .vimrc

# neovim
mkdir ~/.config/nvim
ln -s ~/myconf/vimrc.conf ~/.config/nvim/init.vim


# bash
echo "source ~/myconf/bash.conf" >> ~/.bashrc

# inputrc
ln -s ~/myconf/myinputrc.conf ~/.inputrc
