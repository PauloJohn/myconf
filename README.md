# Vim Configurations:

##Ruby configuration
- gem install neovim
Python
##Python configuration
- create a new virtualenv in your home using:
```
$ virtualenv -p /usr/bin/python3 ~/myvim_env
$ cd ~/myvim_env
$ source bin/activate
$ pip install -r ~/myconf/requirements.txt
```

##Javascript configuration
npm install -D eslint

- run in your home and choose the option to answer some questions
./node_modules/.bin/eslint --init

##Ctags
apt install exuberant-ctags

##Golang configuration
:GoInstallBinaries

## Configure global gitignore
git config --global core.excludesfile '~/myconf/mygitignore'

